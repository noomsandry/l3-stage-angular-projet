import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpTokenService } from './http-token.service';


@Injectable()
export class ServeurService {

  
  constructor( private http : HttpTokenService) {
}

  public getList(){
      let serveur_list = JSON.parse(localStorage.getItem('serveur_list'));
      serveur_list = false
      if(serveur_list){
        return Promise.resolve(serveur_list);
      }else{
        let promise = this.http.get('/admin/serveurs');
        promise.then(data =>{
         // localStorage.setItem('serveur_list' , JSON.stringify(data));
        })
        return promise
      }
 
  }

  public getUser(id){
    //let serveur_list = JSON.parse(localStorage.getItem('serveur_list'));
    // if(serveur_list){
    //   return Promise.resolve(serveur_list);
    // }else{
      let promise = this.http.get('/admin/users/serveur?serveur='+id);
      return promise;
      // promise.then(data =>{
      //   localStorage.setItem('serveur_list' , JSON.stringify(data));
      // })
      // return promise
    }



  public add(serveur) : Promise<any>{

      

      let data = {
        active : serveur.active,
        adresse : serveur.adresse,
        db : serveur.db,
        description : serveur.description,
        login : serveur.login,
        name : serveur.name,
        password : serveur.password,
        port : serveur.port,
        session_id : serveur.session_id
      }
    
      let response =  this.http.post('/admin/serveurs/new', data);
      response.then(res=> {
        if(res){
          let serveur_list = JSON.parse(localStorage.getItem('serveur_list'));
          serveur_list.push(res)
          localStorage.setItem('serveur_list' , JSON.stringify(serveur_list));
        }
      }).catch()
          
      return response;

  }


  public update(serveur) : Promise<any>{
    
          
    
          let data = {
            active : serveur.active,
            adresse : serveur.adresse,
            db : serveur.db,
            description : serveur.description,
            login : serveur.login,
            name : serveur.name,
            password : serveur.password,
            port : serveur.port,
            session_id : serveur.session_id
          }
        
          let response =  this.http.put('/admin/serveurs/'+serveur.id, data);
          response.then(res=> {
            if(res){
              let serveur_list = JSON.parse(localStorage.getItem('serveur_list'));
              serveur_list.push(res)
              localStorage.setItem('serveur_list' , JSON.stringify(serveur_list));
            }
          })
              
          return response;
    
      }

  public remove(idArray){
    return this.http.delete('/admin/serveurs/delete' , {
        ids : idArray
    });
  }

}
