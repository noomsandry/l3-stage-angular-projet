import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { HttpTokenService } from './http-token.service';
import { AuthenticationService } from './authentication.service';
import { User } from '../models/user.model';


@Injectable()
export class UserService {
    constructor(
        private http: HttpTokenService) {
    }

    public getList(){
        // let user_list = JSON.parse(localStorage.getItem('user_list'));
        // if(user_list){
        //   return Promise.resolve(user_list);
        // }else{
          let promise = this.http.get('/admin/users/odoo');
          // promise.then(data =>{
          //   localStorage.setItem('user_list' , JSON.stringify(data));
          // })
          return promise
        //}
    }

    public add(user){
      return this.http.post('/admin/users/new/odoo' , {
        serveur : user.serveur,
        login : user.login
      });
    }

    public remove(idArray){
      return this.http.delete('/admin/users/delete' , {
          ids : idArray
      });
    }
}