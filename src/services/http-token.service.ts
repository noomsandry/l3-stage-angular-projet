import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Globals } from './globas';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class HttpTokenService {

  constructor(private http: Http,private authenticationService: AuthenticationService , private globals : Globals , private router: Router) {
}

  public get(url:string) : Promise<any>{
      // add authorization header with jwt token

      let token =  localStorage.getItem('id_token');
      let headers = new Headers({ 'Authorization': 'Bearer ' + token });
      let options = new RequestOptions({ headers: headers });

      // get users from api
      return this.http.get(this.globals.baseUrl+url, options)
          .map((response: Response) => response.json())
          .toPromise().catch(error =>{
             if(error._body == '{"code":401,"message":"Expired JWT Token"}'){
                this.router.navigate(['/auth']);
             }
      })
          
  }


  public post(url:string , data) : Promise<any>{
    // add authorization header with jwt token

    let token =  localStorage.getItem('id_token');
    let headers = new Headers({ 'Authorization': 'Bearer ' + token , 'Content-Type':'application/json'});
    let options = new RequestOptions({ headers: headers });

    // get users from api
    return this.http.post(this.globals.baseUrl+url, data , options )
        .map((response: Response) => response.json())
        .toPromise().catch(error =>{
          if(error._body == '{"code":401,"message":"Expired JWT Token"}'){
            this.router.navigate(['/auth']);
          }
   })

   
        
}


 public put(url:string , data) : Promise<any>{
    // add authorization header with jwt token

    let token =  localStorage.getItem('id_token');
    let headers = new Headers({ 'Authorization': 'Bearer ' + token , 'Content-Type':'application/json'});
    let options = new RequestOptions({ headers: headers });

    // get users from api
    return this.http.put(this.globals.baseUrl+url, data , options )
        .map((response: Response) => response.json())
        .toPromise().catch(error =>{
          if(error._body == '{"code":401,"message":"Expired JWT Token"}'){
            this.router.navigate(['/auth']);
          }
   })

   
        
}


public delete(url:string , _data) : Promise<any>{
  // add authorization header with jwt token

  let token =  localStorage.getItem('id_token');
  let headers = new Headers({ 'Authorization': 'Bearer ' + token , 'Content-Type':'application/json'});
  let options = new RequestOptions({ headers: headers , body : _data});

  // get users from api
  return this.http.delete(this.globals.baseUrl+url , options )
      .map((response: Response) => response.json())
      .toPromise().catch(error =>{
        if(error._body == '{"code":401,"message":"Expired JWT Token"}'){
          this.router.navigate(['/auth']);
        }
 })

}
}