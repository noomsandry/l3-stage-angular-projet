import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import { OdooRPCService } from 'angular5-odoo-jsonrpc'
import { Globals } from './globas';

@Injectable()
export class OdooService {
   
    constructor(private http: Http , private odooService: OdooRPCService , private globals : Globals) {
        
    }

    public get_list(host) : Promise<any>{
        
        let token =  localStorage.getItem('id_token');
        let headers = new Headers({ 'Authorization': 'Bearer ' + token });
        let options = new RequestOptions({ headers: headers });
  
        // get users from api
        return this.http.get(this.globals.baseUrl+'/admin/serveurs/db?host='+host, options)
            .map((response: Response) => response.json())
            .toPromise().catch(error =>{
               if(error._body == '{"code":401,"message":"Expired JWT Token"}'){
                   
               }
        })
    
    }
        

}