import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthComponent } from '../app/auth/auth.component';


@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private router: Router) { }
  
      canActivate() {
          
          if (localStorage.getItem('currentUser')) {
              // logged in so return true
              return true;
          }
  
          // not logged in so redirect to login page
          this.router.navigate(['/auth']);
          return false;
      }
}
