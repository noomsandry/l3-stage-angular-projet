import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Globals } from './globas';

@Injectable()
export class AuthenticationService {
    public token: string;
    public refresh_token : string;

    constructor(private http: Http , private globals : Globals) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Promise<any> {
        return this.http.post(this.globals.baseUrl+'/login', { _username: username, _password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                let refresh_token = response.json() && response.json().refresh_token;
                if (token && refresh_token) {
                    // set token property
                    this.token = token;
                    this.refresh_token = refresh_token;
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                    localStorage.setItem('id_token', token);
                    localStorage.setItem('refresh_token', refresh_token);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }).toPromise()
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.refresh_token = null;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('id_token');
        localStorage.removeItem('refresh_token');
    }

    do_refresh_token(){
        this.refresh_token =  localStorage.getItem('refresh_token');
        return this.http.post(this.globals.baseUrl+'/api/token/refresh', { refresh_token: this.refresh_token })
        .map((response: Response) => {
            // login successful if there's a jwt token in the response
            let token = response.json() && response.json().token;
            let refresh_token = response.json() && response.json().refresh_token;
            if (this.token && this.refresh_token) {
                // set token property
                this.token = token;
                this.refresh_token = refresh_token;

                localStorage.removeItem('id_token');
                localStorage.removeItem('refresh_token');

                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('id_token', token);
                localStorage.setItem('refresh_token', refresh_token);

                
                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        })
      }
}