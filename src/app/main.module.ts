import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';

// component
import { MainComponent } from './main/main.component';


const appRoutes : Routes = [
  {path: 'auth' , loadChildren : 'app/auth.module#AuthModule' },
  {path: 'app' , loadChildren : 'app/app.module#AppModule' },
  { path: '**', redirectTo: 'app/serveur' }
]


@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [

    BrowserAnimationsModule,
    
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class MainModule { }
