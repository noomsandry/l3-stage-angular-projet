import { Component, OnInit , ViewChild  } from '@angular/core';
import { ServeurService} from '../../services/serveur.service'
import {MatTableDataSource, MatSort , MatPaginator} from '@angular/material';
import { MzModalModule } from 'ng2-materialize'
import { Serveur } from '../../models/serveur.model';
import {OdooService} from '../../services/odoo.service';
import {FormControl, FormGroup} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';


declare var jquery:any;
declare var $:any;


@Component({
  selector: 'app-serveur',
  templateUrl: './serveur.component.html',
  styleUrls: ['./serveur.component.css']
})
export class ServeurComponent implements OnInit {

  private showLoad = false
  // table serveur
  private serveurs : Serveur[]
  private serveur : Serveur = new Serveur()


  displayedColumns = ['select','id', 'name', 'adresse', 'port','description'];
  dataSource;
  selection = new SelectionModel<Serveur>(true, []);
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  // Form add serveur
  private dbList : Array<String>
  private cancelbtn = false
  private showAddBtn = true

  // modal add serveur
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
      //alert('Ready');
      //console.log(modal, trigger);
      $('select').material_select();
      $('#description').trigger('autoresize')

    },
    complete: () => { 
            if(!this.cancelbtn){
                this.serveur.db = $('select').val()
                if(this.serveur.name && this.serveur.adresse && this.serveur.adresse && this.serveur.login && this.serveur.password && this.serveur.port  && this.serveur.db){
                  $('#load-save').show()
                  this.serveur.active = true;
                  this.serveurService.add(this.serveur).then(res => {
                      $('#load-save').hide()
                      if(res){
                        Materialize.toast('Ajouter avec success' ,3000 ,'rounded' )  
                        $('#load-save').hide()
                        this.serveurs.push(res)
                        this.dataSource = new MatTableDataSource(this.serveurs);
                        this.dataSource.sort = this.sort;
                      }else{
                       Materialize.toast('Erreur d\'enregistrement' ,3000 ,'rounded' )  
                      }
                    }).catch(error => {
                       Materialize.toast('Erreur d\'enregistrement' ,3000 ,'rounded' )
                      $('#load-save').hide()
                    } )
                    
                  
                }else{
                  Materialize.toast('Veillez completer tous les champs' ,3000 ,'rounded' )
                }
                this.cancelbtn = false
          }
    } // Callback for Modal close
  };

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatSort) sort: MatSort;

  /**
   * Set the sort after the view init since this component will
   * be able to query its view for the initialized sort.
   */
  ngAfterViewInit() {
    
  }

  constructor(private serveurService : ServeurService , private odooService : OdooService) {
    
    if(typeof(this.serveurs) == 'undefined'){
      this.isLoadingResults = true;
      serveurService.getList().then(data => {
        this.serveurs = data;
        this.dataSource = new MatTableDataSource(this.serveurs);
        this.dataSource.sort = this.sort;

        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.resultsLength = data.length;
      }).catch(error => {
        this.isLoadingResults = false;
        // Catch if the GitHub API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
      })
    }
      

    this.serveur.active = true;
    this.serveur.session_id = ""

   }

  ngOnInit() {
    this.serveur = new Serveur()
  }

  save_change(){
    $('#load-save').show()
    this.serveurService.update(this.serveur).then(res => {
      $('#load-save').hide()
      Materialize.toast('Le serveur est ajoure' ,3000 ,'rounded' )

    })
  }

  get_db_list(){
    if(this.serveur.adresse && this.serveur.port){

      
      this.showLoad = true
        this.odooService.get_list(this.serveur.adresse+':'+this.serveur.port).then(res => {
          
          if(res){
            res = JSON.parse(res);
            this.dbList = res.result;
            var $select = $('select').empty().html('');
            $select.append($(' <option value="" disabled selected>Choisir une base</option>'));
            this.dbList.forEach(element => {
                $select.append($('<option></option>').attr('value',element).text(element));
            });

            $('select').material_select();
            this.showLoad = false
            $('.add-serve-dbpass').slideDown(); 
          }
            
        }).catch(error => {
          console.log(error)
          this.showLoad = false
        })
    }
  }

  onSubmit(){

  }

  save(){
    
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    
    
    if(this.isAllSelected()){
      $('#btn-change-serveur').hide()
    }else{
      $('#btn-change-serveur').show()
    }

    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  show_delete(){
    if(this.selection.selected.length == 1){
      $('#btn-change-serveur').show()
    }else if(this.selection.selected.length == 0){
      $('#btn-change-serveur').hide()
    }
  }

  delete(){
    
    let selectedArray = this.selection.selected.map((e) => {
        return e.id
    })
    
    let res = []
    this.serveurs.forEach((e) => {
      if(!this.in(e.id , selectedArray))
        res.push(e)
    })
    
    this.serveurs = res;
    this.dataSource = new MatTableDataSource(this.serveurs);
    this.dataSource.sort = this.sort;

    this.serveurService.remove(selectedArray).then(res => {
     
      if(res.length > 0){
        Materialize.toast('Le serveur est supprimé success' ,3000 ,'rounded' )
      }
    }).catch(erro => {

    })


    this.masterToggle()

    /// delete serveur
  }

  private in(elem:any , array:Array<any>){
    let trouver = false
    array.forEach(e => {
        if(elem == e)
          trouver = true
    })
    return trouver
  }

  get_info(){
    let selected = this.selection.selected[0]
    this.serveur = selected
    this.showAddBtn = false
  }

}
