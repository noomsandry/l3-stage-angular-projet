import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service'
import { Router } from '@angular/router';

@Component({
  selector: 'auth-root',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  
  constructor(private router: Router, private authenticationService: AuthenticationService) { }


  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    $('#load-auth').show()
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
        .then(result => {
            if (result === true) {
                this.router.navigate(['/serveur']);
            } else {
                this.error = 'Username or password is incorrect';
                this.loading = false;
            }
        }).catch(error => {
            $('#load-auth').hide()
            Materialize.toast('Login et/ou Mot de passe sont incorrect' ,3000 ,'rounded' )  
        })
}

}
