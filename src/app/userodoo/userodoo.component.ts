import { Component, OnInit , ViewChild  } from '@angular/core';
import { UserService } from '../../services/user.service'
import {MatTableDataSource, MatSort , MatPaginator} from '@angular/material';
import { MzModalModule } from 'ng2-materialize'
import { User } from '../../models/user.model'
import { Serveur } from '../../models/serveur.model'
import { ServeurService} from '../../services/serveur.service'
import { DomSanitizer } from '@angular/platform-browser';
import {SelectionModel} from '@angular/cdk/collections';





@Component({
  selector: 'app-userodoo',
  templateUrl: './userodoo.component.html',
  styleUrls: ['./userodoo.component.css']
})
export class UserodooComponent implements OnInit {

  private users : User[]
  private serveurs : Serveur
  private user : User = new User()
  displayedColumns = ['select','id', 'nom', 'login', 'serveur','email'];
  selection = new SelectionModel<Serveur>(true, []);
  dataSource;
  showLoad = true
  private showUserList:boolean = false
  private dataUser = []
  isLoadingResults = false;
  isRateLimitReached = false;

  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
      //alert('Ready');
      //console.log(modal, trigger);
      $('select').material_select();
      
    },
    complete: () => {
    } // Callback for Modal close
  };

  constructor(private userService : UserService , private serveurService : ServeurService , private domSanitizer: DomSanitizer) { 
    if(typeof(this.users) == 'undefined'){
      userService.getList().then(data => {
        this.users = data;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.sort = this.sort;

              
    })

  }

    this.isLoadingResults = true;
    this.serveurService.getList().then(res => {
      if(res){
        this.serveurs =  res
        this.showLoad = false
      }

      this.isLoadingResults = false;
      this.isRateLimitReached = false;

    }).catch(error => {
      this.isLoadingResults = false;
      // Catch if the GitHub API has reached its rate limit. Return empty data.
      this.isRateLimitReached = true;
    })

  
}

applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}

@ViewChild(MatSort) sort: MatSort;

/**
 * Set the sort after the view init since this component will
 * be able to query its view for the initialized sort.
 */
ngAfterViewInit() {}
  


  ngOnInit() {

  }

  getUsers(id_serveur){
    $('#load-user').show()
    
    this.serveurService.getUser(id_serveur).then(res=>{
      
      if(res){
        
            let dataTemp = []
            res.forEach((item, index) => {
                 dataTemp[item.login] = 'data:image/png;base64,'+ item.image_small
            })

            
            this.dataUser = dataTemp
            $('input.autocomplete').autocomplete({
              data : this.dataUser,
              limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
              onAutocomplete: function(val) {
                // Callback function when value is autcompleted.
              },
              minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
            });
            
            $('#autocompletuser').show()
           
            
      }
      $('#load-user').hide()
    }).catch(error =>{
       
    })
  }

  onSubmit(){
    
      }

  add(){
    $('#load-save').show()
    this.user.login = $('#autocomplete-input').val()+"";
    this.userService.add(this.user).then(res => {
      $('#load-save').hide()
      if(res){
        console.log(res)
        this.users.push(res);
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.sort = this.sort;
        Materialize.toast('L\'Utilisateur est ajouté avec success' ,3000 ,'rounded' )
        
      }else{
        Materialize.toast('Erreur d\'enregistrement' ,3000 ,'rounded' )
        
      }
    }).catch(error => {
      $('#load-save').hide()
    })
  }
    
      
  cancel(){
    $('#btn-close').trigger('click')
    
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    
    
    if(this.isAllSelected()){
      $('#btn-delete-user').hide()
    }else{
      $('#btn-delete-user').show()
      
    }
    
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  show_delete(){
    
    if(this.selection.selected.length == 1){
      $('#btn-delete-user').show()
    }else if(this.selection.selected.length == 0){
      $('#btn-delete-user').hide()
    }
  }

  delete(){
    
    let selectedArray = this.selection.selected.map((e) => {
        return e.id
    })
    
    let res = []
    this.users.forEach((e) => {
      if(!this.in(e.id , selectedArray))
        res.push(e)
    })
    
    this.users = res;
    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.sort = this.sort;

    this.userService.remove(selectedArray).then(res => {
     
      if(res.length > 0){
        Materialize.toast('L\'Utilisateur est supprimé success' ,3000 ,'rounded' )
      }
    }).catch(erro => {

    })


    this.masterToggle()

    /// delete serveur
  }

 

  private in(elem:any , array:Array<any>){
    let trouver = false
    array.forEach(e => {
        if(elem == e)
          trouver = true
    })
    return trouver
  }

}
