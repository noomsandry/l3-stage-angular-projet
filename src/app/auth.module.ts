import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { MaterializeModule } from 'angular2-materialize';
import { FormsModule} from '@angular/forms'
import { OdooRPCService } from 'angular5-odoo-jsonrpc'

// material
import { MzSidenavModule } from 'ng2-materialize'
import { MzNavbarModule } from 'ng2-materialize'
import { MzProgressModule } from 'ng2-materialize'
import { MzCardModule } from 'ng2-materialize'
import { MzInputModule} from 'ng2-materialize'
import { MzCheckboxModule} from 'ng2-materialize'
import { MzButtonModule} from 'ng2-materialize'
import { MzModalModule } from 'ng2-materialize'
import { MzValidationModule } from 'ng2-materialize'




import {MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule} from '@angular/material';


// component

import { AuthComponent } from './auth/auth.component';




//service
import { ServeurService } from '../services/serveur.service'
import { AuthGuardService } from '../services/auth-guard.service';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { Globals } from '../services/globas'
import { HttpTokenService } from '../services/http-token.service'
import { OdooService } from '../services/odoo.service'

const appRoutes : Routes = [
  {path: '**' , component : AuthComponent },
]


@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [

    
    HttpModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild(appRoutes),
    MaterializeModule,
    MzSidenavModule,
    MzNavbarModule,
    MzProgressModule,
    MzCardModule,
    MzInputModule,
    MzCheckboxModule,
    MzButtonModule,
    MzModalModule,
    MzValidationModule,


    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule
 
  ],
  providers: [Globals  ,OdooRPCService,  OdooService, ServeurService,HttpTokenService ,AuthGuardService , AuthenticationService , UserService  ],
  bootstrap: [AuthComponent]
})
export class AuthModule { }
