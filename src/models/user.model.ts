export class User {
    id:number
    nom: string
    email: string
    login: string
    serveur: string
    image_small: string
    phone:string
}