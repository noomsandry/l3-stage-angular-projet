export class Serveur {
    id:number
    name: string
    adresse:string
    login:string
    password:string
    port:number
    description:string
    db:string
    active:boolean
    session_id:string
}