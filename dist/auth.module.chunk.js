webpackJsonp(["auth.module"],{

/***/ "../../../../../src/app/auth.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_materialize__ = __webpack_require__("../../../../angular2-materialize/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular5_odoo_jsonrpc__ = __webpack_require__("../../../../angular5-odoo-jsonrpc/odoorpc.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular5_odoo_jsonrpc___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular5_odoo_jsonrpc__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__ = __webpack_require__("../../../../ng2-materialize/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_materialize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_materialize__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_auth_component__ = __webpack_require__("../../../../../src/app/auth/auth.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_serveur_service__ = __webpack_require__("../../../../../src/services/serveur.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_auth_guard_service__ = __webpack_require__("../../../../../src/services/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_authentication_service__ = __webpack_require__("../../../../../src/services/authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_user_service__ = __webpack_require__("../../../../../src/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_globas__ = __webpack_require__("../../../../../src/services/globas.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_http_token_service__ = __webpack_require__("../../../../../src/services/http-token.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_odoo_service__ = __webpack_require__("../../../../../src/services/odoo.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// material










// component

//service







var appRoutes = [
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_9__auth_auth_component__["a" /* AuthComponent */] },
];
var AuthModule = (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__auth_auth_component__["a" /* AuthComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["HttpClientModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forChild(appRoutes),
                __WEBPACK_IMPORTED_MODULE_4_angular2_materialize__["a" /* MaterializeModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzSidenavModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzNavbarModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzProgressModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzCardModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzInputModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzCheckboxModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzButtonModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzModalModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng2_materialize__["MzValidationModule"],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["a" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["c" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["d" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["e" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["f" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["g" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["h" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["i" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["j" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["k" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["l" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["m" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["n" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["o" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["p" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["q" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["r" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["s" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["t" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["u" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["v" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["x" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["w" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["y" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["A" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["D" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["E" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["F" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["G" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material__["B" /* MatStepperModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_14__services_globas__["a" /* Globals */], __WEBPACK_IMPORTED_MODULE_6_angular5_odoo_jsonrpc__["OdooRPCService"], __WEBPACK_IMPORTED_MODULE_16__services_odoo_service__["a" /* OdooService */], __WEBPACK_IMPORTED_MODULE_10__services_serveur_service__["a" /* ServeurService */], __WEBPACK_IMPORTED_MODULE_15__services_http_token_service__["a" /* HttpTokenService */], __WEBPACK_IMPORTED_MODULE_11__services_auth_guard_service__["a" /* AuthGuardService */], __WEBPACK_IMPORTED_MODULE_12__services_authentication_service__["a" /* AuthenticationService */], __WEBPACK_IMPORTED_MODULE_13__services_user_service__["a" /* UserService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9__auth_auth_component__["a" /* AuthComponent */]]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "../../../../../src/app/auth/auth.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#login-block{\r\n    position: relative;\r\n    margin-top: 10%;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"progress\" style=\"margin: 0px;display: none\" id=\"load-auth\">\n  <div class=\"indeterminate\"></div>\n</div>\n<div style=\"background-color:#ee6e73;position: absolute;display: block;width: 100%;height: 100%\"> \n\n</div>\n<div class=\"container\">\n  <div class=\"row\">\n      \n    <mz-card class=\"blue-grey darken-1 white-text col s6 offset-s3\" [hoverable]=\"true\" id=\"login-block\">\n      <mz-card-content> \n          <form name=\"form\" (ngSubmit)=\"f.form.valid && login()\" #f=\"ngForm\" novalidate>\n            <div class=\"row\">\n                <mz-input-container >\n                    <i mz-icon-mdi mz-input-prefix class=\"material-icons\">person</i>\n                    <input mz-input\n                      [validate]=\"true\"\n                      [dataError]=\"'Login obligatoire'\"\n                      id=\"login-input\"\n                      label=\"Login\"\n                      type=\"text\"\n                      name=\"login\"\n                      [(ngModel)]=\"model.username\"\n                       #username=\"ngModel\"\n                       required\n                       mz-validation\n                      >\n                </mz-input-container>\n                <mz-input-container>\n                    <i mz-icon-mdi mz-input-prefix class=\"material-icons\">lock</i>\n                    <input mz-input\n                      [validate]=\"true\"\n                      [dataError]=\"'Mot de passe obligatoire'\"\n                      id=\"login-input\"\n                      label=\"Mot de passe\"\n                      type=\"password\"\n                      name = \"password\"\n                      [(ngModel)]=\"model.password\"\n                       #password=\"ngModel\"\n                       required\n                       mz-validation\n                      >\n                </mz-input-container>\n            </div>\n            <a>Mot de passe oublié</a>\n            <div  class=\"row\" style=\"margin-top:30px\">\n              <button mz-button class=\"col s12\">\n                    Se connecter \n              </button>\n            </div>\n          </form>\n          \n      </mz-card-content>\n\n    </mz-card>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/auth/auth.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_authentication_service__ = __webpack_require__("../../../../../src/services/authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthComponent = (function () {
    function AuthComponent(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.model = {};
        this.loading = false;
        this.error = '';
    }
    AuthComponent.prototype.ngOnInit = function () {
        this.authenticationService.logout();
    };
    AuthComponent.prototype.login = function () {
        var _this = this;
        $('#load-auth').show();
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .then(function (result) {
            if (result === true) {
                _this.router.navigate(['/serveur']);
            }
            else {
                _this.error = 'Username or password is incorrect';
                _this.loading = false;
            }
        }).catch(function (error) {
            $('#load-auth').hide();
            Materialize.toast('Login et/ou Mot de passe sont incorrect', 3000, 'rounded');
        });
    };
    AuthComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'auth-root',
            template: __webpack_require__("../../../../../src/app/auth/auth.component.html"),
            styles: [__webpack_require__("../../../../../src/app/auth/auth.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */], __WEBPACK_IMPORTED_MODULE_1__services_authentication_service__["a" /* AuthenticationService */]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ })

});
//# sourceMappingURL=auth.module.chunk.js.map