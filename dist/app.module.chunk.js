webpackJsonp(["app.module"],{

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".fill{\r\n    width: 100%;\r\n    height: 100%;\r\n    display: inline-block;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <mat-toolbar color=\"primary\">\n    <span>API ODOO</span>\n</mat-toolbar> -->\n\n<div *ngIf=\"showMenu\">\n    <mz-navbar>\n        <!-- <button  class=\"button-collapse\"> -->\n                <mz-navbar-item-container [align]=\"'left'\">\n                    <mz-navbar-item><a id=\"btn-sidenav-demo\"><i class=\"material-icons\">menu</i></a></mz-navbar-item>\n                </mz-navbar-item-container>\n        <!-- </button> -->\n        <a href=\"#\" class=\"brand-logo\"> API ODOO</a>\n        <mz-navbar-item-container [align]=\"'right'\">\n        <mz-navbar-item><a href=\"#\">Accueil</a></mz-navbar-item>\n        <mz-navbar-item><a href=\"#\">Documentation</a></mz-navbar-item>\n        <mz-navbar-item><a routerLink=\"js\">A propos</a></mz-navbar-item>\n        </mz-navbar-item-container>\n    </mz-navbar>\n\n\n    <mz-sidenav [id]=\"'sidenav-demo'\" [collapseButtonId]=\"'btn-sidenav-demo'\"  #sidenav>\n        <mz-sidenav-header>\n                <img src=\"../assets/nav-header.png\" style=\"width:100%\"/>\n        </mz-sidenav-header>\n        <mz-sidenav-subheader>Menu</mz-sidenav-subheader>\n        <mz-sidenav-link [active]=\"active\">\n            <a routerLink=\"/app/serveur\" class=\"waves-effect\" (click)=\"sidenav.opened = !sidenav.opened ; active = !active\"><i class=\"material-icons\">public</i> Serveur</a>\n        </mz-sidenav-link>\n        <mz-sidenav-link [active]=\"!active\">\n            <a routerLink=\"/app/userodoo\" class=\"waves-effect\" (click)=\"sidenav.opened = !sidenav.opened ; active = !active\"><i class=\"material-icons\">person</i>Utilisateur</a>\n        </mz-sidenav-link>\n        <mz-sidenav-divider></mz-sidenav-divider>\n        <mz-sidenav-link [active]=\"!active\">\n                <a routerLink=\"/auth\" class=\"waves-effect\" ><i class=\"material-icons\">power_settings_new</i>Déconnexion</a>\n        </mz-sidenav-link>\n    </mz-sidenav>\n</div>\n\n<router-outlet></router-outlet> \n<footer class=\"page-footer\">\n    <div class=\"footer-copyright\">\n      <div class=\"container\">\n      © 2018 Copyright Madasofware\n      </div>\n    </div>\n  </footer>\n      "

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_authentication_service__ = __webpack_require__("../../../../../src/services/authentication.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(auth) {
        this.auth = auth;
        this.title = 'app';
        this.active = true;
        console.log('eeeeekjkjhkjhk');
        this.showMenu = true;
        /* let route = window.location.href;
         if(route.indexOf('login') != -1) this.showMenu = false
         // auth.do_refresh_token().subscribe(res => {
           
         // })
        */
        var this1 = this;
        setInterval(function () {
            this1.auth.do_refresh_token().subscribe(function (res) {
            });
        }, 1000 * 60 * 50);
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_authentication_service__["a" /* AuthenticationService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_materialize__ = __webpack_require__("../../../../angular2-materialize/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular5_odoo_jsonrpc__ = __webpack_require__("../../../../angular5-odoo-jsonrpc/odoorpc.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular5_odoo_jsonrpc___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular5_odoo_jsonrpc__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__ = __webpack_require__("../../../../ng2-materialize/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_materialize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_materialize__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__serveur_serveur_component__ = __webpack_require__("../../../../../src/app/serveur/serveur.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__userodoo_userodoo_component__ = __webpack_require__("../../../../../src/app/userodoo/userodoo.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_serveur_service__ = __webpack_require__("../../../../../src/services/serveur.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_auth_guard_service__ = __webpack_require__("../../../../../src/services/auth-guard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_authentication_service__ = __webpack_require__("../../../../../src/services/authentication.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_user_service__ = __webpack_require__("../../../../../src/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_globas__ = __webpack_require__("../../../../../src/services/globas.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_http_token_service__ = __webpack_require__("../../../../../src/services/http-token.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_odoo_service__ = __webpack_require__("../../../../../src/services/odoo.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// material










// component



//service







var appRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
        children: [
            { path: 'serveur', component: __WEBPACK_IMPORTED_MODULE_11__serveur_serveur_component__["a" /* ServeurComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_14__services_auth_guard_service__["a" /* AuthGuardService */]] },
            { path: 'userodoo', component: __WEBPACK_IMPORTED_MODULE_12__userodoo_userodoo_component__["a" /* UserodooComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_14__services_auth_guard_service__["a" /* AuthGuardService */]] },
        ]
    }
    // {
    //   path:'',
    //   redirectTo:'/serveur',
    //   pathMatch : 'full'
    // }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__serveur_serveur_component__["a" /* ServeurComponent */],
                __WEBPACK_IMPORTED_MODULE_12__userodoo_userodoo_component__["a" /* UserodooComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["HttpClientModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forChild(appRoutes),
                __WEBPACK_IMPORTED_MODULE_5_angular2_materialize__["a" /* MaterializeModule */],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzSidenavModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzNavbarModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzProgressModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzCardModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzInputModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzCheckboxModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzButtonModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzModalModule"],
                __WEBPACK_IMPORTED_MODULE_8_ng2_materialize__["MzValidationModule"],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["a" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["b" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["c" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["d" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["e" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["f" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["g" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["h" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["i" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["j" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["k" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["l" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["m" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["n" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["o" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["p" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["q" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["r" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["s" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["t" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["u" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["v" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["x" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["w" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["y" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["A" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["D" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["E" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["F" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["G" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material__["B" /* MatStepperModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_17__services_globas__["a" /* Globals */], __WEBPACK_IMPORTED_MODULE_7_angular5_odoo_jsonrpc__["OdooRPCService"], __WEBPACK_IMPORTED_MODULE_19__services_odoo_service__["a" /* OdooService */], __WEBPACK_IMPORTED_MODULE_13__services_serveur_service__["a" /* ServeurService */], __WEBPACK_IMPORTED_MODULE_18__services_http_token_service__["a" /* HttpTokenService */], __WEBPACK_IMPORTED_MODULE_14__services_auth_guard_service__["a" /* AuthGuardService */], __WEBPACK_IMPORTED_MODULE_15__services_authentication_service__["a" /* AuthenticationService */], __WEBPACK_IMPORTED_MODULE_16__services_user_service__["a" /* UserService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/serveur/serveur.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n  \r\n  .example-header {\r\n    min-height: 64px;\r\n    padding: 8px 24px 0;\r\n  }\r\n  \r\n  .mat-form-field {\r\n    font-size: 14px;\r\n    width: 100%;\r\n  }\r\n  \r\n  .mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n\r\n  \r\n.mat-column-select {\r\n  overflow: visible;\r\n}\r\n\r\n.example-loading-shade {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  bottom: 56px;\r\n  right: 0;\r\n  background: rgba(0, 0, 0, 0.15);\r\n  z-index: 1;\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n  -webkit-box-pack: center;\r\n      -ms-flex-pack: center;\r\n          justify-content: center;\r\n}\r\n\r\n.example-rate-limit-reached {\r\n  color: #980000;\r\n  max-width: 360px;\r\n  text-align: center;\r\n}\r\n\r\n.mat-column-number,\r\n.mat-column-state {\r\n  max-width: 64px;\r\n}\r\n\r\n.mat-column-created {\r\n  max-width: 124px;\r\n}\r\n\r\n#btn-change-serveur{\r\n  display: none\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/serveur/serveur.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"progress\" style=\"margin: 0px; display:none\" id=\"load-save\">\n    <div class=\"indeterminate\"></div>\n</div>\n\n<div class=\"example-loading-shade\"\n*ngIf=\"isLoadingResults || isRateLimitReached\">\n<mat-spinner *ngIf=\"isLoadingResults\"></mat-spinner>\n<div class=\"example-rate-limit-reached\" *ngIf=\"isRateLimitReached\">\n    Erreur de chargement de données\n</div>\n</div>\n\n<mz-card class=\"grey lighten-3 black-text\" [hoverable]=\"true\" style=\"margin:10px\">\n    \n  <mz-card-title>\n    Serveur\n  </mz-card-title>\n  <mz-card-content>\n       \n    <div class=\"table-container mat-elevation-z8\">\n\n        <div class=\"example-header\">\n            \n              <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Recherche\">\n            \n          </div>\n\n\n    \n\n      <mat-table #table [dataSource]=\"dataSource\" matSort>\n\n        <ng-container matColumnDef=\"select\">\n            <mat-header-cell *matHeaderCellDef>\n                <mat-checkbox (change)=\"$event ? masterToggle() : null\"\n                            [checked]=\"selection.hasValue() && isAllSelected()\"\n                            [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\n                </mat-checkbox>\n            </mat-header-cell>\n            <mat-cell *matCellDef=\"let row\">\n                <mat-checkbox (click)=\"$event.stopPropagation();\"\n                            (change)=\"$event ? selection.toggle(row) : null; show_delete()\"\n                            [checked]=\"selection.isSelected(row)\">\n                </mat-checkbox>\n            </mat-cell>\n        </ng-container>\n\n          <ng-container matColumnDef=\"id\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Id </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.id}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"name\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Point de vente </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"adresse\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Adresse </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.adresse}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"port\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Port </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.port}} </mat-cell>\n          </ng-container>\n          <ng-container matColumnDef=\"description\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Description </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.description}} </mat-cell>\n          </ng-container>\n\n\n\n            <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n            <mat-row *matRowDef=\"let row; columns: displayedColumns;\"  (click)=\"selection.toggle(row)\"></mat-row>\n      </mat-table>\n      <mat-paginator #paginator\n      [pageSize]=\"10\"\n      [pageSizeOptions]=\"[5, 10, 20]\">\n    </mat-paginator>\n    </div>\n\n    </mz-card-content>\n</mz-card>\n\n<form name=\"form\" (ngSubmit)=\"f.form.valid && save()\" #f=\"ngForm\" novalidate>\n    <mz-modal #bottomSheetModal [options]=\"modalOptions\" fixedFooter = true id=\"modal-add-serveur\">\n        \n    <mz-modal-header>\n      Ajouter un serveur\n    </mz-modal-header>\n    <mz-modal-content>\n    <div class=\"row\">\n        <div class=\"col s12\">\n            <mz-input-container >\n                <input mz-input\n                    [validate]=\"true\"\n                    [dataError]=\"'Nom du point de vente est obligatoire'\"\n                    id=\"name-input\"\n                    label=\"Nom du point de vente\"\n                    type=\"text\"\n                    name=\"name\"\n                    [(ngModel)]=\"serveur.name\" \n                    required\n                    mz-validation\n                    >\n            </mz-input-container>\n        </div>\n    </div>\n        \n    <div class=\"row\">\n        <div class=\"col s6\">\n            <mz-input-container>\n                <input mz-input\n                    [validate]=\"true\"\n                    [dataError]=\"'Adresse doit commancer par http:// ou https://'\"\n                    id=\"adresse-input\"\n                    label=\"Adresse du serveur\"\n                    type=\"text\"\n                    name=\"adresse\"\n                    [(ngModel)]=\"serveur.adresse\" \n                    (blur)=\"get_db_list()\"\n                    required \n                    pattern=\"(http|https)://.+\"\n                    mz-validation\n                    >\n            </mz-input-container>\n        </div>\n        <div class=\"col s6\">\n            <mz-input-container >\n                <input mz-input\n                    [validate]=\"true\"\n                    [dataError]=\"'Port non valide'\"\n                    id=\"port-input\"\n                    label=\"Port\"\n                    type=\"number\"\n                    [(ngModel)]=\"serveur.port\" \n                    (blur)=\"get_db_list()\"\n                    name=\"port\"\n                    mz-validation\n                    maxlength=\"4\"\n                    required\n                    >\n            </mz-input-container>\n        </div>    \n    </div>\n    <div class=\"preloader-wrapper big active\" style=\"display: block;margin: auto;\" *ngIf=\"showLoad\">\n            <div class=\"spinner-layer spinner-blue-only\">\n              <div class=\"circle-clipper left\">\n                <div class=\"circle\"></div>\n              </div><div class=\"gap-patch\">\n                <div class=\"circle\"></div>\n              </div><div class=\"circle-clipper right\">\n                <div class=\"circle\"></div>\n              </div>\n            </div>\n    </div>\n    <div class=\"row add-serve-dbpass\">\n            <div class=\"input-field col s4\">\n                <select [(ngModel)]=\"serveur.db\" name= \"db\">\n                    <option value=\"\" disabled selected>Choisir une base</option>\n                    <option *ngFor=\"let db of dbList\">{{ db }}</option>\n                </select>\n                <label>Base de données</label>\n            </div>\n            <div class=\"col s4\">\n                    <mz-input-container >\n                            <input mz-input\n                                [validate]=\"true\"\n                                [dataError]=\"'Port non valide'\"\n                                id=\"login-input\"\n                                label=\"Login\"\n                                type=\"text\"\n                                name=\"login\"\n                                [(ngModel)]=\"serveur.login\" \n                                required\n                                mz-validation\n                                >\n                    </mz-input-container>\n            </div>\n            <div class=\"col s4\">\n                    <mz-input-container >\n                            <input mz-input\n                                [validate]=\"true\"\n                                [dataError]=\"'Port non valide'\"\n                                id=\"password-input\"\n                                label=\"Password\"\n                                type=\"password\"\n                                name=\"password\"\n                                [(ngModel)]=\"serveur.password\" \n                                required\n                                mz-validation\n                                >\n                    </mz-input-container>\n            </div>\n    </div>\n    <div class=\"row add-serve-dbpass\">\n            <div class=\"input-field col s12\">\n                    <textarea id=\"description\" name=\"description\" class=\"materialize-textarea\" [(ngModel)]=\"serveur.description\" ></textarea>\n                    <label for=\"description\">Description</label>\n            </div>\n    </div>\n    \n\n        \n\n\n    </mz-modal-content>\n    <mz-modal-footer>\n      <button mz-modal-close (click)=\"cancelbtn = true\" mz-button [flat]=\"true\">Annuler</button>\n      <button mz-modal-close  mz-button [flat]=\"true\" [disabled]=\"!f.form.valid\" *ngIf=\"!showAddBtn\" (click)=\"save_change()\">Valider</button>\n      <button mz-button [flat]=\"true\" [disabled]=\"!f.form.valid\" mz-modal-close *ngIf=\"showAddBtn\">Ajouter</button>\n    </mz-modal-footer>\n\n  </mz-modal>\n</form>    \n<!-- <button mz-button class=\"btn-floating btn-large\" >\n    <i class=\"large material-icons\">add</i>\n</button> -->\n<a class=\"btn-floating btn-large\" float=true id=\"btn-add-serveur\" (click)=\"bottomSheetModal.open() ; showAddBtn = true ; ngOnInit()\">\n    <i class=\"large material-icons\">add</i>\n</a>\n\n\n\n<div class=\"fixed-action-btn\" id=\"btn-change-serveur\">\n    <a class=\"btn-floating btn-large\">\n      <i class=\"large material-icons\">call_to_action</i>\n    </a>\n    <ul>\n      <li><a class=\"btn-floating red\"><i class=\"material-icons\" (click)=\"get_info() ; bottomSheetModal.open()\">mode_edit</i></a></li>\n      <li><a class=\"btn-floating yellow darken-1\" id=\"btn-delete-serveur\" (click)=\"delete()\"><i class=\"material-icons\">delete</i></a></li>\n    </ul>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/serveur/serveur.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServeurComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_serveur_service__ = __webpack_require__("../../../../../src/services/serveur.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_serveur_model__ = __webpack_require__("../../../../../src/models/serveur.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_odoo_service__ = __webpack_require__("../../../../../src/services/odoo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ServeurComponent = (function () {
    function ServeurComponent(serveurService, odooService) {
        var _this = this;
        this.serveurService = serveurService;
        this.odooService = odooService;
        this.showLoad = false;
        this.serveur = new __WEBPACK_IMPORTED_MODULE_3__models_serveur_model__["a" /* Serveur */]();
        this.displayedColumns = ['select', 'id', 'name', 'adresse', 'port', 'description'];
        this.selection = new __WEBPACK_IMPORTED_MODULE_5__angular_cdk_collections__["a" /* SelectionModel */](true, []);
        this.resultsLength = 0;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.cancelbtn = false;
        this.showAddBtn = true;
        // modal add serveur
        this.modalOptions = {
            dismissible: false,
            opacity: .5,
            inDuration: 300,
            outDuration: 200,
            startingTop: '100%',
            endingTop: '10%',
            ready: function (modal, trigger) {
                //alert('Ready');
                //console.log(modal, trigger);
                $('select').material_select();
                $('#description').trigger('autoresize');
            },
            complete: function () {
                if (!_this.cancelbtn) {
                    _this.serveur.db = $('select').val();
                    if (_this.serveur.name && _this.serveur.adresse && _this.serveur.adresse && _this.serveur.login && _this.serveur.password && _this.serveur.port && _this.serveur.db) {
                        $('#load-save').show();
                        _this.serveur.active = true;
                        _this.serveurService.add(_this.serveur).then(function (res) {
                            $('#load-save').hide();
                            if (res) {
                                Materialize.toast('Ajouter avec success', 3000, 'rounded');
                                $('#load-save').hide();
                                _this.serveurs.push(res);
                                _this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](_this.serveurs);
                                _this.dataSource.sort = _this.sort;
                            }
                            else {
                                Materialize.toast('Erreur d\'enregistrement', 3000, 'rounded');
                            }
                        }).catch(function (error) {
                            Materialize.toast('Erreur d\'enregistrement', 3000, 'rounded');
                            $('#load-save').hide();
                        });
                    }
                    else {
                        Materialize.toast('Veillez completer tous les champs', 3000, 'rounded');
                    }
                    _this.cancelbtn = false;
                }
            } // Callback for Modal close
        };
        if (typeof (this.serveurs) == 'undefined') {
            this.isLoadingResults = true;
            serveurService.getList().then(function (data) {
                _this.serveurs = data;
                _this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](_this.serveurs);
                _this.dataSource.sort = _this.sort;
                _this.isLoadingResults = false;
                _this.isRateLimitReached = false;
                _this.resultsLength = data.length;
            }).catch(function (error) {
                _this.isLoadingResults = false;
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                _this.isRateLimitReached = true;
            });
        }
        this.serveur.active = true;
        this.serveur.session_id = "";
    }
    ServeurComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    ServeurComponent.prototype.ngAfterViewInit = function () {
    };
    ServeurComponent.prototype.ngOnInit = function () {
        this.serveur = new __WEBPACK_IMPORTED_MODULE_3__models_serveur_model__["a" /* Serveur */]();
    };
    ServeurComponent.prototype.save_change = function () {
        $('#load-save').show();
        this.serveurService.update(this.serveur).then(function (res) {
            $('#load-save').hide();
            Materialize.toast('Le serveur est ajoure', 3000, 'rounded');
        });
    };
    ServeurComponent.prototype.get_db_list = function () {
        var _this = this;
        if (this.serveur.adresse && this.serveur.port) {
            this.showLoad = true;
            this.odooService.get_list(this.serveur.adresse + ':' + this.serveur.port).then(function (res) {
                if (res) {
                    res = JSON.parse(res);
                    _this.dbList = res.result;
                    var $select = $('select').empty().html('');
                    $select.append($(' <option value="" disabled selected>Choisir une base</option>'));
                    _this.dbList.forEach(function (element) {
                        $select.append($('<option></option>').attr('value', element).text(element));
                    });
                    $('select').material_select();
                    _this.showLoad = false;
                    $('.add-serve-dbpass').slideDown();
                }
            }).catch(function (error) {
                console.log(error);
                _this.showLoad = false;
            });
        }
    };
    ServeurComponent.prototype.onSubmit = function () {
    };
    ServeurComponent.prototype.save = function () {
    };
    ServeurComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    ServeurComponent.prototype.masterToggle = function () {
        var _this = this;
        if (this.isAllSelected()) {
            $('#btn-change-serveur').hide();
        }
        else {
            $('#btn-change-serveur').show();
        }
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    ServeurComponent.prototype.show_delete = function () {
        if (this.selection.selected.length == 1) {
            $('#btn-change-serveur').show();
        }
        else if (this.selection.selected.length == 0) {
            $('#btn-change-serveur').hide();
        }
    };
    ServeurComponent.prototype.delete = function () {
        var _this = this;
        var selectedArray = this.selection.selected.map(function (e) {
            return e.id;
        });
        var res = [];
        this.serveurs.forEach(function (e) {
            if (!_this.in(e.id, selectedArray))
                res.push(e);
        });
        this.serveurs = res;
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](this.serveurs);
        this.dataSource.sort = this.sort;
        this.serveurService.remove(selectedArray).then(function (res) {
            if (res.length > 0) {
                Materialize.toast('Le serveur est supprimé success', 3000, 'rounded');
            }
        }).catch(function (erro) {
        });
        this.masterToggle();
        /// delete serveur
    };
    ServeurComponent.prototype.in = function (elem, array) {
        var trouver = false;
        array.forEach(function (e) {
            if (elem == e)
                trouver = true;
        });
        return trouver;
    };
    ServeurComponent.prototype.get_info = function () {
        var selected = this.selection.selected[0];
        this.serveur = selected;
        this.showAddBtn = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatSort */])
    ], ServeurComponent.prototype, "sort", void 0);
    ServeurComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-serveur',
            template: __webpack_require__("../../../../../src/app/serveur/serveur.component.html"),
            styles: [__webpack_require__("../../../../../src/app/serveur/serveur.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_serveur_service__["a" /* ServeurService */], __WEBPACK_IMPORTED_MODULE_4__services_odoo_service__["a" /* OdooService */]])
    ], ServeurComponent);
    return ServeurComponent;
}());



/***/ }),

/***/ "../../../../../src/app/userodoo/userodoo.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n  \r\n  .example-header {\r\n    min-height: 64px;\r\n    padding: 8px 24px 0;\r\n  }\r\n  \r\n  .mat-form-field {\r\n    font-size: 14px;\r\n    width: 100%;\r\n  }\r\n  \r\n  .mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n\r\n  #load-user , #autocompletuser{\r\n    display:none\r\n  }\r\n\r\n  .example-loading-shade {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    bottom: 56px;\r\n    right: 0;\r\n    background: rgba(0, 0, 0, 0.15);\r\n    z-index: 1;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n  }\r\n  \r\n  .example-rate-limit-reached {\r\n    color: #980000;\r\n    max-width: 360px;\r\n    text-align: center;\r\n  }\r\n\r\n  .mat-column-number,\r\n  .mat-column-state {\r\n    max-width: 64px;\r\n  }\r\n  \r\n  .mat-column-created {\r\n    max-width: 124px;\r\n  }\r\n\r\n  .mat-column-select {\r\n    overflow: visible;\r\n  }\r\n \r\n  #btn-delete-user{\r\n    display: none\r\n  }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/userodoo/userodoo.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"progress\" style=\"margin: 0px; display:none\" id=\"load-save\">\n    <div class=\"indeterminate\"></div>\n</div>\n\n<div class=\"example-loading-shade\"\n*ngIf=\"isLoadingResults || isRateLimitReached\">\n<mat-spinner *ngIf=\"isLoadingResults\"></mat-spinner>\n<div class=\"example-rate-limit-reached\" *ngIf=\"isRateLimitReached\">\n    Erreur de chargement de données\n</div>\n</div>\n\n<mz-card class=\"grey lighten-3 black-text\" [hoverable]=\"true\" style=\"margin:10px\">\n  <mz-card-title>\n    Utilisateur\n  </mz-card-title>\n  <mz-card-content>\n  \n    <div class=\"table-container mat-elevation-z8\">\n\n        <div class=\"example-header\">\n            \n              <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Recherche\">\n            \n          </div>\n\n      <mat-table #table [dataSource]=\"dataSource\" matSort>\n        <ng-container matColumnDef=\"select\">\n                <mat-header-cell *matHeaderCellDef>\n                    <mat-checkbox (change)=\"$event ? masterToggle() : null \"\n                                [checked]=\"selection.hasValue() && isAllSelected()\"\n                                [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\n                    </mat-checkbox>\n                </mat-header-cell>\n                <mat-cell *matCellDef=\"let row\">\n                    <mat-checkbox (click)=\"$event.stopPropagation()\"\n                                (change)=\"$event ? selection.toggle(row) : null;show_delete()\"\n                                [checked]=\"selection.isSelected(row)\">\n                    </mat-checkbox>\n                </mat-cell>\n            </ng-container>\n            \n          <ng-container matColumnDef=\"id\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Id </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.id}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"nom\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Nom </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.nom}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"login\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Login </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.login}} </mat-cell>\n          </ng-container>\n\n          <ng-container matColumnDef=\"serveur\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Point de vente </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.serveur}} </mat-cell>\n          </ng-container>\n          <ng-container matColumnDef=\"email\">\n              <mat-header-cell *matHeaderCellDef mat-sort-header> Email </mat-header-cell>\n              <mat-cell *matCellDef=\"let element\"> {{element.email}} </mat-cell>\n          </ng-container>\n          \n\n\n\n            <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n            <mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"selection.toggle(row)\"></mat-row>\n      </mat-table>\n      <mat-paginator #paginator\n      [pageSize]=\"10\"\n      [pageSizeOptions]=\"[5, 10, 20]\">\n    </mat-paginator>\n    </div>\n\n    </mz-card-content>\n</mz-card>\n\n\n<mz-modal #bottomSheetModal [options]=\"modalOptions\">\n  <mz-modal-header>\n    Ajouter un utilisateur\n  </mz-modal-header>\n  <mz-modal-content *ngIf=\"showLoad\">\n    <div class=\"preloader-wrapper big active\" style=\"display: block;margin: auto;\">\n            <div class=\"spinner-layer spinner-blue-only\">\n            <div class=\"circle-clipper left\">\n                <div class=\"circle\"></div>\n            </div><div class=\"gap-patch\">\n                <div class=\"circle\"></div>\n            </div><div class=\"circle-clipper right\">\n                <div class=\"circle\"></div>\n            </div>\n            </div>\n    </div>\n  </mz-modal-content>\n  <mz-modal-content *ngIf=\"!showLoad\">\n\n        <div class=\"row\">\n                <div class=\"input-field col s12\">\n                    <i class=\"material-icons prefix\">home</i>\n                    <select [(ngModel)]=\"user.serveur\" (ngModelChange)=\"getUsers($event)\" materialize=\"material_select\">\n                        <option value=\"\" disabled selected>Choisir le point de vente</option>\n                        <option *ngFor=\"let s of serveurs\" [ngValue]=\"s.id\">{{ s.name }}</option>\n                    </select>\n                    <label>Point de vente</label>\n                </div>\n        </div>\n        \n        <div class=\"preloader-wrapper big active\" style=\"margin: auto;\"  id=\"load-user\">\n                <div class=\"spinner-layer spinner-blue-only\">\n                <div class=\"circle-clipper left\">\n                    <div class=\"circle\"></div>\n                </div><div class=\"gap-patch\">\n                    <div class=\"circle\"></div>\n                </div><div class=\"circle-clipper right\">\n                    <div class=\"circle\"></div>\n                </div>\n                </div>\n        </div>\n\n        \n      \n        <div class=\"row\" id=\"autocompletuser\">\n            <div class=\"input-field col s12\">\n                <i class=\"material-icons prefix\">people</i>\n                <input type=\"text\" id=\"autocomplete-input\" class=\"autocomplete\" >\n                <label for=\"autocomplete-input\">Utilisateur de odoo</label>\n            </div>\n        </div>\n\n\n      \n\n\n  </mz-modal-content>\n  <mz-modal-footer>\n    <button mz-button [flat]=\"true\" mz-modal-close>Annuler</button>\n    <button mz-button [flat]=\"true\" mz-modal-close (click)=\"add()\">Valider</button>\n  </mz-modal-footer>\n</mz-modal>\n\n\n\n\n<!-- <button mz-button float=true >\n  <i class=\"material-icons\">add</i>\n</button> -->\n\n<a class=\"btn-floating btn-large\" float=true id=\"btn-add-user\" (click)=\"bottomSheetModal.open()\">\n        <i class=\"large material-icons\">add</i>\n</a>\n\n<a class=\"btn-floating btn-large\" float=true id=\"btn-delete-user\" (click)=\"delete()\">\n    <i class=\"large material-icons\">delete</i>\n</a>\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/userodoo/userodoo.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserodooComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_user_model__ = __webpack_require__("../../../../../src/models/user.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_serveur_service__ = __webpack_require__("../../../../../src/services/serveur.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserodooComponent = (function () {
    function UserodooComponent(userService, serveurService, domSanitizer) {
        var _this = this;
        this.userService = userService;
        this.serveurService = serveurService;
        this.domSanitizer = domSanitizer;
        this.user = new __WEBPACK_IMPORTED_MODULE_3__models_user_model__["a" /* User */]();
        this.displayedColumns = ['select', 'id', 'nom', 'login', 'serveur', 'email'];
        this.selection = new __WEBPACK_IMPORTED_MODULE_6__angular_cdk_collections__["a" /* SelectionModel */](true, []);
        this.showLoad = true;
        this.showUserList = false;
        this.dataUser = [];
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.modalOptions = {
            dismissible: false,
            opacity: .5,
            inDuration: 300,
            outDuration: 200,
            startingTop: '100%',
            endingTop: '10%',
            ready: function (modal, trigger) {
                //alert('Ready');
                //console.log(modal, trigger);
                $('select').material_select();
            },
            complete: function () {
            } // Callback for Modal close
        };
        if (typeof (this.users) == 'undefined') {
            userService.getList().then(function (data) {
                _this.users = data;
                _this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](_this.users);
                _this.dataSource.sort = _this.sort;
            });
        }
        this.isLoadingResults = true;
        this.serveurService.getList().then(function (res) {
            if (res) {
                _this.serveurs = res;
                _this.showLoad = false;
            }
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
        }).catch(function (error) {
            _this.isLoadingResults = false;
            // Catch if the GitHub API has reached its rate limit. Return empty data.
            _this.isRateLimitReached = true;
        });
    }
    UserodooComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    UserodooComponent.prototype.ngAfterViewInit = function () { };
    UserodooComponent.prototype.ngOnInit = function () {
    };
    UserodooComponent.prototype.getUsers = function (id_serveur) {
        var _this = this;
        $('#load-user').show();
        this.serveurService.getUser(id_serveur).then(function (res) {
            if (res) {
                var dataTemp_1 = [];
                res.forEach(function (item, index) {
                    dataTemp_1[item.login] = 'data:image/png;base64,' + item.image_small;
                });
                _this.dataUser = dataTemp_1;
                $('input.autocomplete').autocomplete({
                    data: _this.dataUser,
                    limit: 20,
                    onAutocomplete: function (val) {
                        // Callback function when value is autcompleted.
                    },
                    minLength: 1,
                });
                $('#autocompletuser').show();
            }
            $('#load-user').hide();
        }).catch(function (error) {
        });
    };
    UserodooComponent.prototype.onSubmit = function () {
    };
    UserodooComponent.prototype.add = function () {
        var _this = this;
        $('#load-save').show();
        this.user.login = $('#autocomplete-input').val() + "";
        this.userService.add(this.user).then(function (res) {
            $('#load-save').hide();
            if (res) {
                console.log(res);
                _this.users.push(res);
                _this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](_this.users);
                _this.dataSource.sort = _this.sort;
                Materialize.toast('L\'Utilisateur est ajouté avec success', 3000, 'rounded');
            }
            else {
                Materialize.toast('Erreur d\'enregistrement', 3000, 'rounded');
            }
        }).catch(function (error) {
            $('#load-save').hide();
        });
    };
    UserodooComponent.prototype.cancel = function () {
        $('#btn-close').trigger('click');
    };
    UserodooComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    UserodooComponent.prototype.masterToggle = function () {
        var _this = this;
        if (this.isAllSelected()) {
            $('#btn-delete-user').hide();
        }
        else {
            $('#btn-delete-user').show();
        }
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    UserodooComponent.prototype.show_delete = function () {
        if (this.selection.selected.length == 1) {
            $('#btn-delete-user').show();
        }
        else if (this.selection.selected.length == 0) {
            $('#btn-delete-user').hide();
        }
    };
    UserodooComponent.prototype.delete = function () {
        var _this = this;
        var selectedArray = this.selection.selected.map(function (e) {
            return e.id;
        });
        var res = [];
        this.users.forEach(function (e) {
            if (!_this.in(e.id, selectedArray))
                res.push(e);
        });
        this.users = res;
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["C" /* MatTableDataSource */](this.users);
        this.dataSource.sort = this.sort;
        this.userService.remove(selectedArray).then(function (res) {
            if (res.length > 0) {
                Materialize.toast('L\'Utilisateur est supprimé success', 3000, 'rounded');
            }
        }).catch(function (erro) {
        });
        this.masterToggle();
        /// delete serveur
    };
    UserodooComponent.prototype.in = function (elem, array) {
        var trouver = false;
        array.forEach(function (e) {
            if (elem == e)
                trouver = true;
        });
        return trouver;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatSort */])
    ], UserodooComponent.prototype, "sort", void 0);
    UserodooComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-userodoo',
            template: __webpack_require__("../../../../../src/app/userodoo/userodoo.component.html"),
            styles: [__webpack_require__("../../../../../src/app/userodoo/userodoo.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__services_serveur_service__["a" /* ServeurService */], __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */]])
    ], UserodooComponent);
    return UserodooComponent;
}());



/***/ }),

/***/ "../../../../../src/models/serveur.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Serveur; });
var Serveur = (function () {
    function Serveur() {
    }
    return Serveur;
}());



/***/ }),

/***/ "../../../../../src/models/user.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    return User;
}());



/***/ })

});
//# sourceMappingURL=app.module.chunk.js.map